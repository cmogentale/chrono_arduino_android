package roller.chartres.cmatic.chronostartstop2lines;

/**
 * Created by cmatic on 27/07/17.
 */

public class Utils {
    public static String formatDoubleDuree(double dureeNano)
    {
        double d=dureeNano/1000000000;
        int secondes=(int)d;
        String reste=Double.valueOf(  dureeNano-secondes*1000000000).toString().replace(".","");
        int minutes=(int)(secondes/60);
        secondes=(int)(((double)secondes)-60*(double)minutes);
        return Integer.valueOf(minutes).toString()+"''"+Integer.valueOf(secondes).toString()+"'"+reste.substring(0,Math.min(3,reste.length()-1));
    }
}
