package roller.chartres.cmatic.chronostartstop2lines;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Set;
import java.util.UUID;

import roller.chartres.cmatic.chronostartstop2lines.R;

import static android.view.Window.FEATURE_NO_TITLE;

public class ChronoLigne extends AppCompatActivity {


    public int isBluetoothSetup = 0;
    public BluetoothAdapter mBluetoothAdapter;
    long timeStart=0;
    long timeStop=0;
    double meilleurChrono=Double.POSITIVE_INFINITY;

    Handler hStart;
    private String macAddressStart="";
    private TextView fenetreInfos;
    private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final String TAG = "chrono2";


    final int RECEIVE_MESSAGE = 1;    //1    // Status  for Handler
    private BluetoothSocket btSocketStart = null;

    private ChronoLigne.ConnectedThread mConnectedThreadStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chrono_ligne);
        fenetreInfos=(TextView) findViewById(R.id.infosLigne);

      /*  Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        hStart = new Handler() {
            public void handleMessage(android.os.Message msg) {
                switch (msg.what) {
                    case RECEIVE_MESSAGE:                                   // if receive massage
                        byte[] readBuf = (byte[]) msg.obj;
                        String strIncom = new String(readBuf, 0, msg.arg1); // create string from bytes array
                        strIncom=strIncom.replace("\r","");
                        strIncom=strIncom.replace("\n","");

//                        Toast.makeText(getBaseContext(), "start: longueur:"+Long.toString(strIncom.length()), Toast.LENGTH_LONG).show();

                        //                      Toast.makeText(getBaseContext(), "start: "+strIncom, Toast.LENGTH_LONG).show();

                        if ( strIncom.length()==1 && strIncom.contains("2") ) //ligne coupée chrono start => on demarre le chrono
                        {
                            if(timeStart>0) //chrono deja demarre
                            {
                                timeStop = System.nanoTime();
                                fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,130);
                                double chrono=timeStop - timeStart;
                                if(chrono<meilleurChrono)
                                {
                                    meilleurChrono=chrono;
                                    fenetreInfos.setTextColor(Color.GREEN);

                                }
                                else
                                    fenetreInfos.setTextColor(Color.RED);

                                fenetreInfos.setText(Utils.formatDoubleDuree(chrono));

                                timeStart=0;
                                timeStop=0;
                            }
                                else
                            {
                                fenetreInfos.setTextColor(Color.BLACK);

                                fenetreInfos.setText("....");
                                timeStart = System.nanoTime();
                            }
                        }

                        break;
                }
            }

            ;
        };
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ChronoLigne.this,MainActivity.class);
        startActivity(intent);
        ChronoLigne.this.finish();
    }

    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        if(Build.VERSION.SDK_INT >= 10){
            try {
                final Method m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
                return (BluetoothSocket) m.invoke(device, MY_UUID);
            } catch (Exception e) {
                fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                fenetreInfos.setText("probleme detection de module BT, essayez de les appairer au téléphone de nouveau et de redémarrer l'application");
                Log.e(TAG, "Could not create Insecure RFComm Connection",e);
            }
        }
        return  device.createRfcommSocketToServiceRecord(MY_UUID);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "...bluetooth check...");

        bluetoothCheck();
        // if(isBluetoothSetup==0)
        //   return;
        Log.d(TAG, "...onResume - try connect...");
        if(mBluetoothAdapter==null) return;
        if(macAddressStart.isEmpty()) return;

        // Set up a pointer to the remote node using it's address.
        try{
            BluetoothDevice deviceStart = mBluetoothAdapter.getRemoteDevice(macAddressStart);

            // Two things are needed to make a connection:
            //   A MAC address, which we got above.
            //   A Service ID or UUID.  In this case we are using the
            //     UUID for SPP.

            btSocketStart = createBluetoothSocket(deviceStart);

        } catch (Exception e) {

            errorExit("Fatal Error", "In onResume() and socket create failed: " + e.getMessage() + ".");
        }

        // Discovery is resource intensive.  Make sure it isn't going on
        // when you attempt to connect and pass your message.
        mBluetoothAdapter.cancelDiscovery();

        // Establish the connection.  This will block until it connects.
        Log.d(TAG, "...Connecting...");
        fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        fenetreInfos.setText("connexion au module BT ...");

        try {
            btSocketStart.connect();
            fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,150);
            fenetreInfos.setText(" ---- ");


            Log.d(TAG, "....Connection ok...");
        } catch (Exception e) {
            fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
            fenetreInfos.setText("probleme detection de module BT, essayez de les appairer au téléphone de nouveau et de redémarrer l'application");
            Log.d(TAG,e.getMessage());
            try {
//                btSocketStart.close();
                //              btSocketStop.close();
            } catch (Exception e2) {
                errorExit("Fatal Error", "In onResume() and unable to close socket during connection failure" + e2.getMessage() + ".");
            }
        }

        // Create a data stream so we can talk to server.
        Log.d(TAG, "...Create Socket...");

        mConnectedThreadStart = new ChronoLigne.ConnectedThread(btSocketStart);
        mConnectedThreadStart.setHandler(hStart);
        mConnectedThreadStart.start();
    }

    private void errorExit(String title, String message){

        fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
        fenetreInfos.setText("probleme detection de module BT, essayez de les appairer au téléphone de nouveau et de redémarrer l'application");
    }


    public void bluetoothCheck()
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth, "", Toast.LENGTH_SHORT).show();
            fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);

            fenetreInfos.setText(
                    "Votre téléphone ne supporte pas le bluetooth, c'est ballot !");
        }
        else
        {

            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
            else
                checkBluetoothDevices();
        }
    }

    private void checkBluetoothDevices()
    {
        /*Method getUuidsMethod = null;
        try {
            getUuidsMethod =BluetoothAdapter.class.getDeclaredMethod("getUuids", null);
            ParcelUuid[] uuids = (ParcelUuid[]) getUuidsMethod.invoke(mBluetoothAdapter, null);
          //  uuid1=uuids[0].getUuid();
           // uuid2=uuids[1].getUuid();


        } catch (Exception e) {
            e.printStackTrace();
        }
*/

        Log.d(TAG, "...check bluetooth devices...");

        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        boolean chrstartTrouve=false;

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                if(deviceName.toLowerCase().equals("chrligne")) {
                    chrstartTrouve = true;
                    macAddressStart=device.getAddress();
                }

            }
        }

        if(chrstartTrouve==false) {
            fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);

            fenetreInfos.setText(
                    "le module bluetooth du chronometre de ligne n'a pas été trouvé (nom bluetooth: CHRLIGNE), il faut l'appairer à votre téléphone avant de lancer l'application."
            );
        }
        else
        {
            //tout est ok pour chronométrer !
            isBluetoothSetup=1;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK)
        {
            //bluetooth a été activé, on vérifie que le chrstart et chrstop sont appairés
            checkBluetoothDevices();
        }
        else
        {
            fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);

            fenetreInfos.setText("Sans bluetooth l'application ne fonctionnera pas, vous pouvez maintenant la fermer.");
        }
    }

    private class ConnectedThread extends Thread {
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private Handler h;

        public void setHandler(Handler n){this.h=n;}

        public ConnectedThread(BluetoothSocket socket) {
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams, using temp objects because
            // member streams are final
            try {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
            } catch (Exception e) {
                Log.d(TAG,e.getMessage());
                fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                fenetreInfos.setText("probleme detection de module BT, essayez de les appairer au téléphone de nouveau et de redémarrer l'application");

            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            Log.d(TAG, "...listening...");

            byte[] buffer = new byte[256];  // buffer store for the stream
            int bytes; // bytes returned from read()

            // Keep listening to the InputStream until an exception occurs
            while (true) {
                try {
                    // Read from the InputStream
                    bytes = mmInStream.read(buffer);        // Get number of bytes and message in "buffer"
                    h.obtainMessage(RECEIVE_MESSAGE, bytes, -1, buffer).sendToTarget();     // Send to message queue Handler
                } catch (Exception e) {
                    break;
                }
            }
        }

        /* Call this from the main activity to send data to the remote device */
        public void write(String message) {
            Log.d(TAG, "...Data to send: " + message + "...");
            byte[] msgBuffer = message.getBytes();
            try {
                mmOutStream.write(msgBuffer);
            } catch (Exception e) {
                fenetreInfos.setTextSize(TypedValue.COMPLEX_UNIT_SP,20);
                fenetreInfos.setText("probleme detection de module BT, essayez de les appairer au téléphone de nouveau et de redémarrer l'application");
                Log.d(TAG, "...Error data send: " + e.getMessage() + "...");
            }
        }
    }

}
