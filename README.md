# README #

Ce document présente les différentes parties du projet

### Que fait ce projet et quel est son principe de fonctionnement ? ###


Ce projet permet de créer des chronomètres basés sur un principe similaire :

Un module arduino (dans le dossier arduino à la racine du projet, nom chrono_sol_photocell_couplebt_auto_etal.ino) qui contient une cellule photo électrique et un module bluetooth, et associé à un rayon laser 
(un télémètre laser idéalement)
une cellule photoelectrique (coiffée d'une lentille grossissante pour concentrer le rayon laser placé en face)
lit en permanence la résistance provoquée par ce rayon. Lorsqu'un coureur coupe cette ligne laser
la résistance lue à la cellule photo électrique change et écrit "2" sur le module bluetooth pour signaler
que la ligne a été coupée. le schéma fritzing du circuit arduino s'appelle circuit.rzl à la racine du dépot.

L'application android (dossier app à la racine du dépot) dans le même dépot reçoit ce signal et s'en sert pour arrêter ou commencer un chronométrage. 
A chaque fois qu'un chronométrage se termine le résultat est affiché sur le smartphone via l'appli android.

### Chronomètre tour par tour et chronomètre entre deux lignes ###

Ce projet se compose de deux sous-projets, utilisant chaque fois le même module arduino, 
mais gérés différement  par la même application android:

* un chronomètre de ligne
Utile pour mesurer un tour complet, il se déclenche quand un coureur franchit la ligne du module arduino une premiere fois
et se stoppe à un nouveau passage de cette ligne. Pour fonctionner, le téléphone doit être appairé avec le module 
bluetooth de la platine arduino et ce module doit forcément s'appeler CHRLIGNE (voir la section configuration des 
nom des modules bluetooth).

* un chronomètre de départ et un d'arrivée, composé de deux modules arduino

chaque module arduino contient un module bluetooth et est identique au module arduino du chronomètre de ligne.mais sur le premier module arduino,
le module bluetooth doit se nommer CHRSTART et sur l'autre CHRSTOP, et les deux modules doivent être appairés avec le 
téléphone qui sert à lancer l'application de chronométrage.
pour configurer le nom d'un module bluetooth sur arduino, voir la section suivante :


### Comment forcer le nom du module bluetooth, qui est par défaut HC-05 ? ###

le module bluetooth doit être raccordé à l'arduino d'une certaine façon : voir, à la racine du dépot, le fichier fritzing init_nom_modules_bluetooth.
le code arduino permettant, avec ce circuit, de modifier le nom du module bluetooth connecté, est dans le répertoire arduino à la racine du dépot et se nomme BTConfigWithSerialCommand.ino
pour changer le nom du module bluetooth, 
réalisez le circuit dont le schéma fritzing est donné, et débranchez l'alimentation en 5V du module arduino
avant de brancher le cable usb à l'ordinateur.
une fois le cable usb branché, vous pouvez remettre le 5V au module bluetooth. sa petite led doit clignoter par intervalles longs de deux secondes.

lancer depuis arduino studio le code, placer vous en mode moniteur série et 
envoyez : AT+NAME=CHRSTART si vous voulez renommer votre module en CHRSTART.
en réponse sur le moniteur série vous devez recevoir OK si ça a fonctionné.

plus de détails ici :
http://www.instructables.com/id/Modify-The-HC-05-Bluetooth-Module-Defaults-Using-A/


